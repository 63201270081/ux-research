<!--Use following title format: 
  "Incentives request for #(issue number of your research project)"-->

## To be completed by the requester
* [ ] Make a copy of the following Google doc: https://docs.google.com/spreadsheets/d/1moXi9orRAC2-nmlO-f0P0pnFsZu7PEspBrOjSl-Ooaw/copy
* [ ] Fill out the copied document where relevant.
* [ ] Ensure the relevant UX Research Coordinator can edit the file.
* [ ] Paste the link to the completed, copied document here: 

## To be completed by the UX Research Coordinator
* [ ] Pay users
* [ ] Delete the copied document from Google Drive.

/confidential
/label ~"Research coordination"
/relate #<!--issue number here-->
