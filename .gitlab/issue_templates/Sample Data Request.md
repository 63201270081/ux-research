<!--
This issue type is for requesting sample data to be generated on the [UX Cloud testing envronment](https://ux.gitlabdemo.cloud/), for the purpose of conducting research. If you do not have access to this yet, please follow the directions on [this handbook page](https://about.gitlab.com/handbook/customer-success/demo-systems/#access-shared-omnibus-instances).

Also check the [sample data script repository](https://gitlab.com/leducmills/uxr-dummy-data-scripts) as it contains project files that may suit your needs without additional work.

Please answer the below questions to the best of your ability.
-->

#### What research project(s) is this request associated with? (Please link any issues or other documentation)

#### Who will be the DRI for this research project?

#### Please describe what you intend to use the testing envronment for:
<!--
For example: we will need 20 identical projects to run a usability benchmarking study for the Create stage.
-->


#### Please fill out the following tables to the best of your ability:
<!--

Currently, the following types of data have been successfully generated:
- Groups
- Projects
- Labels
- Issues
- Branches
- Files (as commits)
- Commit Comments
- Merge Requests (max 1 per branch)
- Milestones

More are possible, based on the list of classes in the Gitlab ruby gem [found here](https://www.rubydoc.info/gems/gitlab/Gitlab/Client). Note that this will increase the time needed to fulfill the request.

Example defaults for the various content types can be found in [this sample project](https://gitlab.com/ux-research-sandbox/examples/tanuki-inc-example-1).
-->

| Question | Answer |
|--------|--------|
| Study name (as you wish it to appear in the sandbox) | |
| Dates you want your study to run | |
| How many sub-groups do you need? (under the main study group) | |
| How many projects do you need? (per group) | |
| Will all the projects be the same? | |
| Please list any feature flags you need (only those released to production are available) | |

How many of each of the following do you need? (Please specify per project or per group.)

| Type | Number |
|--------|--------|
| Labels| |
| Issues| |
| Branches| |
| Files (as commits)| |
| Commit Comments| |
| Merge Requests (max 1 per branch)| |
| Milestones | |
| Others | Add new rows as needed |

Please list any data that requires a specific name or value (e.g., you need a branch named 'staging')
| Type | Value |
|--------|--------|
| (e.g., branch name) | (e.g., 'Staging') |


/label ~"UX research"
/label ~"Sample Data Request"
/confidential
