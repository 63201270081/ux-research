<!-- Please do not use this template if you are a: Product Manager, UX Designer or UX Researcher. Instead, please use either the `Problem validation` or `Solution validation` template`. -->

#### What did we learn?

<!-- Add information for this section after the research is complete. -->

| Results |
| ------ |
| `2-3 sentences to summarize the results` |
| - Link to Dovetail project |

#### What’s this issue all about?

#### What hypotheses and/or assumptions do you have?

#### What questions are you trying to answer?

<!-- 
Examples: Are people not paying for a subscription because they don’t like the features we have? Do we have issues with the website? Are our prices too high? Are we missing features that customers are looking for?

- What needs to be answered to move work forward? 

- These aren't the questions you'll directly ask users. 
-->

##### Core questions 

<!-- What needs to be answered to move work forward? -->

##### Additional questions

<!-- Is there anything else you'd like to know? -->

#### What persona, persona segment, or customer type experiences the problem most acutely?

#### What business decisions will be made based on this information?

<!-- Assign this issue to the Product Manager, Product Designer and UX Researcher for the relevant group. If you're unsure of who that is, you can check here:
https://about.gitlab.com/handbook/product/categories/

If there is no one listed or you're unsure, assign the issue to the UX Research Director. Leaving an issue unassigned means it may go unnoticed by the UX Research Team.
-->

/label ~"UX Research Backlog"

/label ~"UX Research request"
