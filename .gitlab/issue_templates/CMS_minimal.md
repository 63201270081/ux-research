<!--Use following title format: 
  "Category Maturity: working towards minimal - (list JTBD here)"-->

## Meeting with your primary persona
When we move a GitLab category from Planned to Minimal, we rely on internal users to provide feedback about whether the category meets the basic needs of the primary persona it is intended to support. This feedback gives us valuable information about next steps to take when defining early requirements for the primary Job to Be Done (JTBD).

## Two easy steps
* Step 1: Identify persona and primary JTBD.
* Step 2: Interview 3-5 GitLab Team Members who align with the role of your primary persona. We recommend recording the session to video.

## The questions to ask
These are the minimum questions you should ask your primary personas when interviewing.  They were designed to provide you with actionable feedback.  

### INSTRUCTIONS
Please fill in the `answer here` sections below in the issue description for each participant you interviewed.

-------------------------------------------
#### Participant 1

Q: _Are you using GitLab to do (JTBD)?_ `answer here`
* **If yes:** 
     Q: _Tell me how that experience has been for you?_ (find out ‘why’) `answer here`

     Q: _Does GitLab have the features you need to do (JTBD)?_ `answer here`
     
     * If no: _Why?_ `answer here`

     Q: _What could we do to improve the experience of (JTBD)?_ `answer here`

* **If no:** Q: _Why not?_ `answer here`

-------------------------------------------
#### Participant 2

Q: _Are you using GitLab to do (JTBD)?_ `answer here`
* **If yes:** 
     Q: _Tell me how that experience has been for you?_ (find out ‘why’) `answer here`

     Q: _Does GitLab have the features you need to do (JTBD)?_ `answer here`
     
     * If no: _Why?_ `answer here`

     Q: _What could we do to improve the experience of (JTBD)?_ `answer here`

* **If no:** Q: _Why not?_ `answer here`

-------------------------------------------
#### Participant 3

Q: _Are you using GitLab to do (JTBD)?_ `answer here`
* **If yes:** 
     Q: _Tell me how that experience has been for you?_ (find out ‘why’) `answer here`

     Q: _Does GitLab have the features you need to do (JTBD)?_ `answer here`
     
     * If no: _Why?_ `answer here`

     Q: _What could we do to improve the experience of (JTBD)?_ `answer here`

* **If no:** Q: _Why not?_ `answer here`
