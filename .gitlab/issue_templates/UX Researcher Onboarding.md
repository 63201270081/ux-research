# Hello :wave:

Welcome to the UX Research team!

We have created this issue to help you get up to speed.

First, it's likely that you want to know where your place is in the company structure. For this, you can check out the [team chart](https://about.gitlab.com/team/chart/).

If you have questions, don't hesitate to mention **ADD BUDDY** (your UX buddy), @sarahj (your Manager), or the whole UX Research team by using `@uxresearchers` (use the latter sparingly unless it's something important). You can also reach out to people in the [#ux research](https://gitlab.slack.com/messages/ux_research/) or [#ux](https://gitlab.slack.com/messages/ux/) Slack channels.

Your onboarding doesn't have a scheduled end date, just focus on getting comfortable with GitLab, learning about your groups and getting to know your colleagues.

# Company :earth\_africa:

- Most of the information you will ever need is documented in GitLab's [handbook](https://about.gitlab.com/handbook/).

- Understanding GitLab's [values](https://about.gitlab.com/handbook/values/) is really important. Our values describe the type of behavior that we expect from the people that we hire. They help us to know how to behave in the organization and what to expect from others. Values are a framework for distributed decision making, they allow you to determine what to do without asking your manager.

- [Learn how we communicate as a company](https://about.gitlab.com/handbook/communication/). We use asynchronous communication as a start and are as open as we can be by communicating through public issues, chat channels, and placing an emphasis on ensuring that conclusions of offline conversations are written down. 

- When setting up your home office, get advice and guidance about what you might need and what you can expense by checking out the [spending company money page](https://about.gitlab.com/handbook/spending-company-money/).

- Finally, the [general guidelines](https://about.gitlab.com/handbook/general-guidelines/) will help you understand how we get things done.

# Vacation :palm\_tree: 

We have a ["no ask, must tell" policy](https://about.gitlab.com/handbook/paid-time-off/). You do not need to ask permission to take time off unless you want to have more than 25 consecutive calendar days off. The 25-day no ask limit is per vacation, not per year.

If you are planning to be on vacation for longer than a day, please communicate your time off by:

- Updating [PTO Ninja](https://about.gitlab.com/handbook/paid-time-off/#pto-ninja) in Slack and changing your Slack status.

- Updating your [status](https://docs.gitlab.com/ee/user/profile/#current-status) in GitLab. 

- Updating the [UX calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV85cHNoMjZmaGEzZTRtdmhscmVmdXNiNjE5a0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t).

- Updating your personal calendar. If you use Google's `Out of Office` event feature any meetings that are scheduled for when you are out of office will be auto-declined.

- Notifying Sarah in your 1:1 meetings.

You should communicate your time off for public holidays (since they differ depending on where you are in the world!).

You don't need to worry about taking an hour or two off to go to the gym, take a nap, go grocery shopping, do household chores, etc. 

## UX Department :raised\_hands:

Each department has its own handbook section, and UX is no exception:
- Read through the whole [UX handbook](https://about.gitlab.com/handbook/engineering/ux/) section, please! Consider this your first team task, understanding the Mission, Vision and how work is executed within the department. If you find any typo's or items that could be made clearer, please consider [editing that page](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/source/handbook/engineering/ux/index.html.md) by submitting a merge request. 

- Schedule coffee chats with other members of the UX department to get to know who you work with. Talk about everyday things. We want you to make friends and build relationships with the people you work with to create a more comfortable, well-rounded environment. 

- If you want to bring something to the attention of all UX Department members, you can ping `@uxers` on Slack, email: `ux-department@gitlab.com` or mention `@gitlab\-com/gitlab\-ux` within GitLab (please use all forms sparingly unless it's something important).

## Your section :fox:

- UX Researchers operate at a [section level](https://about.gitlab.com/handbook/product/categories/#hierarchy).

- Product Designers and Product Managers operate at a group level. 

- **UPDATE SECTION** has been designated as your section. Within **UPDATE SECTION**, you are supporting the following groups: **UPDATE GROUPS LIST**

- Get to know your [teammates](https://about.gitlab.com/handbook/product/categories/). For example, in the **UPDATE GROUP** group, some of your colleagues are **UPDATE PM** (Product Manager), **UPDATE PRODUCT DESIGN MANAGER** (UX/Product Design Manager) and **UPDATE PRODUCT DESIGNER** (Product Designer).

- Read the visions for **[UPDATE SECTION LINK](https://about.gitlab.com/direction/UPDATE)**

- Schedule a time with your [Product Management stable-counterparts](https://about.gitlab.com/handbook/product/categories/#devops-stages) to review the vision.

- Learn about [GitLab's features](https://about.gitlab.com/features/).

- The [Demos](https://about.gitlab.com/handbook/marketing/product-marketing/demo/) page is useful for learning more about how GitLab (as a product) works.

# Staying informed :mega:

- There are over 1000 Slack channels at GitLab. You can join as many as you like! However, you may find the following channels most useful at first:
[#competition](https://gitlab.slack.com/messages/competition/),[#git-help](https://gitlab.slack.com/messages/git-help/), [#peopleops](https://gitlab.slack.com/messages/peopleops/), [#product](https://gitlab.slack.com/messages/product/), [#questions](https://gitlab.slack.com/messages/questions/), [#ux](https://gitlab.slack.com/messages/ux/) and [#ux research](https://gitlab.slack.com/messages/ux_research/). Each group also has a Slack channel. Each group channel normally starts with the letter 'g'. For example, the Growth stage group channel is #g_growth.

- You should have now received an invite for the `Company call` (every day except Friday). If you can't attend the call, don't worry, just check the [company call agenda](https://docs.google.com/document/d/1JiLWsTOm0yprPVIW9W-hM4iUsRxkBt_1bpm3VXV4Muc/edit) in order to stay on top of the most important events.

- There is usually a [group conversation](https://about.gitlab.com/handbook/people-operations/group-conversations/) before each company call. Group Conversations are a way for teams to keep the wider business informed about what they are currently working on. We have a UX Research Group Conversation every 5 weeks where you can share and present some of your research findings.

- We have a weekly [UX meeting](https://docs.google.com/document/d/189WZO7uTlZCznzae2gqLqFn55koNl3-pHvU-eVnvG9c/edit) every Tuesday. 

- We also have a weekly [UX Research meeting](https://docs.google.com/document/d/1OM3eFSwGY-SdjspRN_F-dU43do0swh96u7q0jLstx6c/edit) every Thursday.

- There is an early/late `UX Hangout` call once a month. This is where members of the UX Department can meet informally to talk about anything they want. Check out the UX calendar to find out when the next one is!

- You should now be a member of the [#ux_research_pairs](https://gitlab.slack.com/messages/ux_research_pairs/) Slack channel. Every 2 weeks on a Monday, you'll be randomly paired with another researcher from the team. This is a chance for you to chat, swap tips and advice on research projects, processes, etc. 

- You will have a weekly 1:1 meeting with Sarah. This meeting is for you! Please take a moment before our 1:1s to add items to the agenda. This can be anything from an update on what you are working on to questions and concerns about the team, company, etc.

- Sarah is here to support you! If you need a 1:1 chat at any time, her calendar is open and up to date. You can also DM her on Slack via `@sarah`. 

- You are not obligated to attend any of the meetings that you are invited to. However, please notify others of your absence by responding to Google calendar invites. If you do not attend a meeting, please read the agenda items and go through the notes of the meeting. Also check whether there is a recording available.

## Your onboarding experience :open\_hands: 

This issue is confidential by default, meaning this is a safe space to ask any questions and to describe any experiences you might have had. It's a tool to help **you** succeed!

To share some new found knowledge, the rest of the UX Research team would love to hear your experiences. 

Within this issue, please let us know how we can improve the UX Researcher onboarding experience. Please keep notes on what went well, what didn't go well, or what you feel could be improved. We'll use your feedback to improve the onboarding experience for future UX Researchers who join the team. 

## Closing thoughts :pray:

Taking in a lot of information can quickly feel overwhelming. This is normal and you are encouraged to ask questions at any time. Embrace knowing nothing and you'll take it in as you go. Take your time to work through your onboarding tasks, you are not expected to be at 100% productivity right away! And remember: if you are here, it's because you are the best person we have found in the world (literally) to do this job :smile:

Working remotely comes with great power and great responsibility. [Here are a few tips on being efficient and productive](https://about.gitlab.com/2018/05/17/eliminating-distractions-and-getting-things-done/). 

### Remember to practice self-care

Make sure you take breaks if you feel overwhelmed or tired, especially if this is your first remote job. Remember to stretch your legs, drink some water, and disconnect when necessary. Try to establish a healthy routine that will empower you to work in a way that enables you to deliver the best results.