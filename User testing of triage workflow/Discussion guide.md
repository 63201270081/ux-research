# User testing of triage workflow – Discussion guide
## Session with a participant
### Session introduction
- Introduce people on call
- Ice breaker
- Session agenda
     - Task accomplishment comment
     - Design feedback

Before we start, I would like to remind you, that we will record this session.
The recording will not be published anywhere publicly. I will use it for the analysis purposes and possibly to communicate some interesting findings to the team.

### Part 1: Triage workflow
During the first part of this session I will ask you to accomplish a task in the
GitLab project that I invited you to. Please open the project. 

#### Scenario introduction
Before starting with the task, I would like to mention that:
- This is not an exam or test. In case some things do not work ideally, don't be worried about sharing it. These are exactly the things that we want to learn more about.
- Shortly I will provide you with detailed instructions about how to accomplish it.
I can help clarify if anything is unclear in the instructions, but I won’t 
be able to answer questions or provide any help once you begin.
- Please, while going through the activity, talk about your thought process. It will help us better understand what is going on.

#### Scenario
The scenario for this task is following: 

Imagine, you work on the Operations team for GitLab and you are responsible for maintaining GitLab.com. You use Prometheus to gather metrics and alert on the performance of GitLab.com. 

This week GitLab is hosting a global hackathon and you anticipate more than 100,000 additional users logging onto GitLab.com to participate in the hackathon. You are the team lead and it is your job to keep an eye on your systems to make sure that GitLab.com remains available for paying customers and hackathon participants. This is a huge marketing event for GitLab so it is really important that you address any outages as quickly as possible.

There are two other colleagues on-call today:
* Sarah Waldner, @sarahwaldner
* Amelia Bauerly, @ameliabauerly 

Tools are your disposal:
- GitLab alert list
- Metrics
- Logs
- Ability to create incidents
- Linked Zoom calls

Is everything clear so far?

You just received an alert in Slack from GitLab indicating that something might be wrong. 

- Show participant the ![Slack message screenshot](slack_alert.png).

Please check the alert and triage it using GitLab – determine if you need to create an incident to respond to the problem. If you do create an incident, please respond as you would at your own company.

Prompts: "And what would you do now? ... Please go on."

#### Post-test questions
How did it go?

Would you add details to the incident?
 
Would you involve your team?

- Walk through the process again, mention usability issues that you noticed.

### Part 2: Incident issue feedback
 ❗️Check time. If there is not enough time to go through this part of the session fully, skip it. 

#### Initial questions
How do you keep track of incidents? 

#### Feedback collection
**Description**: In previous task you tried using issues to keep track of incidents. To make the experience even better, we plan to introduce dedicated type of issue that will contain more information than regular issue to make it more suitable for triaging incidents. 

- Show the mockup to participant.

![Mockup](Incident_-_adjusted_UI.png)

**First impression**: This is how will the new incident issue look like. Please tell me you first impressions of it? (Additional questions: What did you notice the first? How do you feel about it? Do you have any ideas about how to improve it)

- Let user talk freely about what they see.

**Walktrough**: Thank you for your feedback. In addition to what you have already mentioned, there are a few more features, that I would like to get your feedback for. 

- Point them to features, explain how do they work and why they are there. Ask if they have any feedback.

#### Final questions

Based on what you have seen, do you think this new design will be useful for you? Will it be a big difference from the approach to tracking incidents you use today?

Do you have any concerns about it or improvement ideas that you did not mention so far?

### Session wrap up
Thank you very much for taking your time and participating in this session.  Your comments will help us to make the new functionality useful to our users.

Bye!

---

## Debriefing
❗ Always conduct debriefing!

- What have we learned
- Was this participant special in any way
